#include <stdio.h>

/* Celsius to Fahrenheit */

main(){

  float fahr, celsius;
  int upper, lower, step;

  lower = -20;
  upper = 160;
  step = 20;

  celsius = lower;

  printf("CEL  FAHR \n--- ------\n");

  while(celsius <= upper){
    fahr = (9.0/5.0) * celsius + 32.0;
    printf("%3.0f %6.1f\n", celsius, fahr);
    celsius += step;
  }

}

/*
Exercise 1-4. Write a program to print the corresponding Celsius to Fahrenheit table.

Answer.

CEL  FAHR 
--- ------
-20   -4.0
  0   32.0
 20   68.0
 40  104.0
 60  140.0
 80  176.0
100  212.0
120  248.0
140  284.0
160  320.0

*/