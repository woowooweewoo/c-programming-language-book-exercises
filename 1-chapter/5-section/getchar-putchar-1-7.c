#include <stdio.h>

main(){
  printf("The value of EOF is %d", EOF);
}

/*
  Exercise 1-7. Write a program to print the value of EOF.

  Answer. The value of EOF is -1.
  
*/