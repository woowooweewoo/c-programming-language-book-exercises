/*
 Exercise 1-8. Write a program to count blanks, tabs, and newlines.

 Answer. See code below:
*/

#include <stdio.h>

/* count blanks, tabs, and newlines in input */
main(){

  int c, blank, tab, newline;

  blank = tab = newline = 0;
  while((c = getchar()) != EOF){
    if(c == ' ') blank++;
    else if(c == '\t') tab++;
    else if(c == '\n') newline++;
  }
  printf("blanks: %d; tabs: %d; newlines: %d;\n", blank, tab, newline);
}