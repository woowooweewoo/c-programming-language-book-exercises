#include <stdio.h>

#define IN 1
#define OUT 0

/* count lines, words, and characters in input */
main(){
  int c, nl, nw, nc, state;

  state = OUT;

  nl = nw = nc = 0;

  while((c = getchar()) != EOF){
    nc++;
    if(c == '\n') nl++;
    if(c == ' ' || c == '\n' || c == '\t'){
      state = OUT;
    } else if(state == OUT) {
      state = IN;
      nw++;
    }
  }

  printf("\nnewline: %d; newword: %d; newchar: %d;\n", nl, nw, nc);
}

/*
  Exercise 1-11. How would you test the word count program? What kinds of
  input are most likely to uncover bugs if there are any?

  Answer. I would test the word count program first with normal input, e.g.
  several sentences, which would reveal obvious bugs. Then I would test
  edge cases like multiple tabs, blanks, or newlines in a row, and mixed
  sequences of them as well, which would reveal rarely occuring bugs.
*/