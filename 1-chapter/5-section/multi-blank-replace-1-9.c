/*
 Exercise 1-9. Write a program to copy its input to its output,
 replacing each string of one or more blanks by a single blank.

 Answer. See code below:
*/

#include <stdio.h>

/* replace multiple sequential blanks in input with single blanks on output */
main(){
  int input, multi;

  while((input = getchar()) != EOF){
    if(input != ' ' && multi == 0){
      putchar(input);
    } else if(input == ' ' && multi == 0){
      multi = 1;
      putchar(input);
    } else if(input != ' ' && multi == 1){
      multi = 0;
      putchar(input);
    }
  }

}