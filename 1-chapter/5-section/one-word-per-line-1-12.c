/*
  Exercise 1-12. Write a program that prints its input one word per line.

  Answer. See code below:
*/

#include <stdio.h>

#define IN_WORD 1
#define OUT_WORD 0

main(){
  int c, state;
  
  state = OUT_WORD;

  while((c = getchar()) != EOF){

    if(c == ' ' || c == '\n' || c == '\t'){
      if(state == IN_WORD){
        state = OUT_WORD;
        putchar('\n');
      }
    } else {
      if(state == OUT_WORD){
        state = IN_WORD;
      }
      putchar(c);
    }

  }
}

