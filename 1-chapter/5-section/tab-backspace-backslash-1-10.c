/*
  Exercise 1-10. Write a program to copy its inputs to its outputs,
  replacing each tab by \t, each backspace by \b (to represent this
  in the shell, use ctrl-H, the ASCII backspace value), and each backslash
  by \\. This makes tabs and backspaces visible in an unambiguous way.

  Answer. See code below:
*/

#include <stdio.h>

main(){
  int c;

  while((c = getchar()) != EOF){
    if(c == '\t'){
      putchar('\\');
      putchar('t');
    } else if(c == '\b'){
      putchar('\\');
      putchar('b');
    } else if(c == '\\'){
      putchar('\\');
      putchar('\\');
    } else {
      putchar(c);
    }
  }
}