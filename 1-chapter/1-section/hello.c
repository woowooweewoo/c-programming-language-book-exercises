#include <stdio.h>

main(){
  printf("hello, world");
}

/*
* Exercise 1-1. Run the "hello, world" program on your system. Experiment with leaving
* out parts of the program, to see what error messages you get.
*
* Answer.
*
* When I left out the ';', I received an error stating that a ';' was expected before a '}' token. 
*
* When I left out the '}', I received an error stating expected declaration or
* statement at end of input.
*
* When I left out '()', I received an error stating expected '=', ',', ';', 'asm',
* or '__attribute__' before '{' token.
*
* When I left out the entire 'main(){' and '}', I received an error stating
* expected declaration specifiers or '...' before string constant.
*
* When I left out '#include <stdio.h>', I received a warning that 'printf' was implicitly declared,
* but the compiler did not throw an error and included the library for me. It also left a note
* that I should include '<stdio.h>' or provide a declaration of 'printf'.
*
*
*
* Exercise 1-2. Experiment to find out what happens when printf's argument string contains \c,
* where c is some character not listed above.
*
* Answer.
*
* I tried \g as an escape character and I received a warning - unknown escape sequence '\g',
* but the program compiled successfully. When I ran the program, it printed 'hello, worldg',
* omitting the '\' and printing the 'g'.
*/