#include <stdio.h>

int power(int m, int n);

/* test power function */
main(){
  int i;

  for(i = 0; i < 10; i++){
    printf("%6d %6d %6d\n", i, power(2,i), power(-3,i));
  }

}

/*
  power: raise base to n-th power; n >= 0;
  This is an example of call-by-value, where we manipulate
  the local copy of parameter n without changing the
  argument passed into the function.
 */
int power(int base, int n){
  int p;

  for(p = 1; n > 0; n--){
    p = p * base;
  }
  return p;
}