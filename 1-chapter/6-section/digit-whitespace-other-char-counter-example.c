#include <stdio.h>

/* count digit, whitespace, other characters example */
main(){
  int c, i, numWhiteSpace, numChar;

  int numDigit[10];

  numWhiteSpace = numChar = 0;

  for(i = 0; i < 10; i++) numDigit[i] = 0;

  while((c = getchar()) != EOF){

    if(c == ' ' || c == '\t' || c == '\n'){
      numWhiteSpace++;
    } else if (c - '0' < 10){
      numDigit[c - '0']++;
    } else {
      numChar++;
    }
  }

  printf("\ndigit count: ");
  for(i = 0; i < 10; i++){
     printf("%d ", numDigit[i]);
  }
  printf("| whitespace count: %d | other char count: %d\n", numWhiteSpace, numChar);
}