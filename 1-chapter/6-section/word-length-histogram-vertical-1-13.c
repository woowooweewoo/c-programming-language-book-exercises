/*
  Exercise 1-13. Write a program to print a histogram of the lengths
  of words in its input. It is easy to draw the histogram with the
  bars horizontal; a vertical orientation is more challenging.

  Answer. See code below (vertical version):
*/

#include <stdio.h>

#define IN 1
#define OUT 0

main(){

  int c, i, j, state, largest;

  int wordLength[10];

  for(i = 0; i < 10; i++){
     wordLength[i] = 0;
  }

  state = OUT;
  i = 0;

  while((c = getchar()) != EOF){
    if(c == ' ' || c == '\t' || c == '\n'){
      if(state == IN && i < 10){
        wordLength[i - 1]++;
        state = OUT;
        i = 0;
      } else if (state == IN && i >= 10){
        wordLength[9]++;
        state = OUT;
        i = 0;
      }
    } else if(i < 10){
      if(state == OUT){
        state = IN;
      }
      i++;
    }
  }

  if(state == IN){
    if(i < 10){
      wordLength[i - 1]++;
    } else {
      wordLength[9]++;
    }
  }

  printf("\n\n");

  largest = 0;

  for(i = 0; i < 10; i++){
    if(largest < wordLength[i]){
      largest = wordLength[i];
    }
  }

  for(i = largest; i > 0; i--){
    for(j = 0; j < 10; j++){
      if(wordLength[j] >= i){
        printf("* ");
      } else {
        printf("  ");
      }
    }
    printf("\n");
  }
  for(i = 0; i < 10; i++){
    if(i == 9){
      printf("10+");
    } else {
      printf("%d ", (i + 1));
    }
  }

  printf(" - word lengths \n");
}

/*
asdf asdf a a a a a sdf asdf as as as asd asd a a a a asdfasdf asdfadsf asdfasdf asdfasdf asdfasdfasdf asdfasfdsadf asdfds

*                   
*                   
*                   
*                   
*                   
*             *     
* * * *       *     
* * * *       *   * 
* * * *   *   *   * 
1 2 3 4 5 6 7 8 9 10+ - word lengths
*/