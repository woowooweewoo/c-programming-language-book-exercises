/*
  Exercise 1-14. Write a program to print a histogram of the
  frequencies of different characters in its input.

  Answer. See code below:
*/

#include <stdio.h>

main(){

  int c, a, d, o, i;

  a = d = o = 0;

  while((c = getchar()) != EOF){
    if(((c - 'A') <= 25 && (c - 'A') >= 0) || ((c - 'a') <= 25 && (c - 'a') >= 0)){
      a++;
    } else if((c - '0') <= 10 && (c - '0') >= 0){
      d++;
    } else {
      o++;
    }
  }

  printf("\n\ncharacter frequency histogram:\n\n");

  printf("a ");
  for(i = 0; i < a; i++){
    printf("*");
  }
  printf("\nd ");
  for(i = 0; i < d; i++){
    printf("*");
  }
  printf("\no ");
  for(i = 0; i < o; i++){
    printf("*");
  }

  printf("\n\nlegend:\n\n");
  printf("a\talphabetic (A-Za-z)\n");
  printf("d\tdigit (0-9)\n");
  printf("o\tother\n");
}

/*
asdfasdfasdfkjasd f  132498(*&)(*^^asdf   21341234adsfasdfdsaf2

character frequency histogram:

a **********************************
d ***************
o **************

legend:

a	alphabetic (A-Za-z)
d	digit (0-9)
o	other
*/