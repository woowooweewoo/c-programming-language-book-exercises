/*
  Exercise 1-15. Rewrite the temperature conversion program of
  Section 1.2 to use a function for conversion.

  Answer. See code below:
*/

#include <stdio.h>

#define LOWER 0
#define UPPER 300
#define STEP 20

float fahrToCel(float f);

main(){
  int fahr = LOWER;

  while(fahr <= UPPER){
    printf("%3d %6.1f\n", fahr, fahrToCel(fahr));
    fahr += STEP;
  }
}

float fahrToCel(float f){
  return (5.0/9.0) * (f - 32.0);
}